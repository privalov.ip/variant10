﻿namespace Variant10.DbModel
{
    public class Player
    {
        public int Id { get; set; }
        public int Top { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Position { get; set; }
        public string TimeOnIce { get; set; }
        public int GameId { get; set; }
        public int TeamId { get; set; }

       public virtual Game Game { get; set; }
       public virtual Team Team { get; set; }
    }
}
