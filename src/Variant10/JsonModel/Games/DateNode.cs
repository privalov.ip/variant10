﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Games
{
    class DateNode
    {
        [JsonPropertyName("date")]
        public string Date { get; set; }
        [JsonPropertyName("games")]
        public List<GameNode> Games { get; set; }
    }
}
