﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class PersonInfo
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("fullName")]
        public string FullName { get; set; }
        [JsonPropertyName("birthDate")]
        public string BirthDate { get; set; }
        [JsonPropertyName("primaryNumber")]
        public string PrimaryNumber { get; set; }
        [JsonPropertyName("currentTeam")]
        public TeamInfo CurrentTeam { get; set; }
        [JsonPropertyName("primaryPosition")]
        public PositionInfo PrimaryPosition { get; set; }
    }
}