﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Variant10.DbModel;

namespace Variant10.DbModelTests
{
    class TestDbHelper<T> where T : class
    {
        private readonly GamesContext gameDbContext;
        
        public TestDbHelper()
        {
            var builder = new DbContextOptionsBuilder<GamesContext>();
            builder.UseInMemoryDatabase(databaseName: "GamesDbInMemory");

            var dbContextOptions = builder.Options;
            gameDbContext = new GamesContext(dbContextOptions);
            gameDbContext.Database.EnsureDeleted();
            gameDbContext.Database.EnsureCreated();
        }

        public async Task<int> Add(T t)
        {
            gameDbContext.Set<T>().Add(t);
            return await gameDbContext.SaveChangesAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await gameDbContext.Set<T>().FindAsync(id);
        }

        public async Task<int> Remove(T t)
        {
            gameDbContext.Set<T>().Remove(t);
            return await gameDbContext.SaveChangesAsync();
        }

        public async Task<int> Update(T t)
        {
            gameDbContext.Set<T>().Update(t);
            return await gameDbContext.SaveChangesAsync();
        }
    }
}
