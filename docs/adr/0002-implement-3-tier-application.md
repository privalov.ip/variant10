# 2. Implementing 3-tier Application

2021-12-04

## Status

Accepted

## Context

The basic requirement for diploma task is implementing a simple application with a 3-tier architecture.

## Decision

I will be using the basic DotNET WebApplication template. It provides all 3 layers in one build. User layer is a web browser with HTML/JS loaded from web server. Logic layer is NhlApiService which fills DB using API. Data layer is a managed DB (RDS with MySQL) from AWS. The main word is simplicity.

## Consequences

DotNET web application is very easy to develop and test. This saves time on diploma work.