#!/bin/bash

# Use this script to set kubectl config using terraform outputs
# Make sure you run 'terraform init' to sync with remote backend before this script


if [ -z "$1" ]
  then
    echo "No config name supplied!"
	exit 1
fi

kubectl config set-cluster k8s --server="$(terraform output cluster_endpoint | tr -d '"')"
kubectl config set clusters.k8s.certificate-authority-data $(terraform output cluster_cert_data | tr -d '"')
kubectl config set-credentials $1 --token="$(terraform output $1_token | tr -d '"')"
kubectl config set-context default --cluster=k8s --user=$1
kubectl config use-context default