# variable "AWS_ACCESS_KEY_ID" {
#   type = string
# }

# variable "AWS_SECRET_ACCESS_KEY" {
#   type = string
# }

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "domain" {
  type    = string
  default = "v10.fun"
}

variable "app_envs" {
  type    = set(string)
  default = ["prod", "dev"]
}

variable "appname" {
  type    = string
  default = "var10-app"
}

variable "helm_path" {
  type    = string
  default = "../helm"
}

variable "cert_secret" {
  type    = string
  default = "var10-nginx-tls"
}

variable "issuer_name" {
  type    = string
  default = "letsencrypt"
}

variable "db_name" {
  type    = string
  default = "var10db"
}

variable "db_user" {
  type    = string
  default = "dbadmin"
}

variable "az_list" {
  description = "AZs in this region"
  default     = ["eu-central-1a", "eu-central-1b"]
  type        = list(any)
}

variable "public_subnet_cidrs" {
  description = "Subnet CIDRs for public subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  type        = list(any)
}

variable "private_subnet_cidrs" {
  description = "Subnet CIDRs for private subnets"
  default     = ["10.0.3.0/24", "10.0.4.0/24"]
  type        = list(any)
}

