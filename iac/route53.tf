#Getting host zone, be sure if it already exist

data "aws_route53_zone" "v10_zone" {
  name         = var.domain
  private_zone = false
}

data "kubernetes_service" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = helm_release.ingress_nginx.metadata[0].namespace
  }
}

data "aws_elb" "ingress_elb" {
  name = regex(
    "(^[^-]+)",
    data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
  )[0]

  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "aws_route53_health_check" "health_route53" {
  fqdn              = var.domain
  port              = 443
  type              = "HTTPS"
  resource_path     = "/"
  failure_threshold = "5"
  request_interval  = "30"

  tags = {
    Name = "var10-route53-health-check"
  }

  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "aws_route53_record" "alias_route53_record" {
  zone_id = data.aws_route53_zone.v10_zone.zone_id
  name    = var.domain
  type    = "A"

  alias {
    name                   = data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
    zone_id                = data.aws_elb.ingress_elb.zone_id
    evaluate_target_health = true
  }

  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "aws_route53_record" "cname_sub_record" {
  zone_id = data.aws_route53_zone.v10_zone.zone_id
  name    = "*"
  type    = "CNAME"
  ttl     = "300"
  records = [data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname]

  depends_on = [
    helm_release.ingress_nginx
  ]
}
