terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14"
}

provider "aws" {
  region = "eu-west-2"
}

resource "aws_s3_bucket" "var10_s3" {
  bucket = "var10-tf-state"
  acl    = "private"

  versioning {
    enabled = true
  }
}
