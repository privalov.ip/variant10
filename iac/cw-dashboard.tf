data "template_file" "dashboard-template" {
  template = file("cw-dashboard.json")
  vars = {
    CLUSTER_NAME = "${local.cluster_name}"
    APP_NAME     = "${var.appname}"
    PROD_NS      = "${kubernetes_namespace.env_ns["prod"].metadata.0.name}"
    DEV_NS       = "${kubernetes_namespace.env_ns["dev"].metadata.0.name}"
    REGION       = "${var.region}"
  }
}

resource "aws_cloudwatch_dashboard" "eks-dashboard" {
  dashboard_name = "${local.cluster_name}-dashboard"
  dashboard_body = data.template_file.dashboard-template.rendered
}
