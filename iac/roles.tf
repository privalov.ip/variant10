resource "kubernetes_cluster_role" "cluster-deployer" {
  metadata {
    name = "cluster-deployer"
  }

  rule {
    api_groups = ["", "apps", "networking.k8s.io", "keda.sh"]
    resources  = ["pods", "deployments", "services", "ingresses", "secrets", "scaledobjects"]
    verbs      = ["get", "list", "create", "update", "patch"]
  }

  rule {
    api_groups = ["", "autoscaling"]
    resources  = ["namespaces", "horizontalpodautoscalers"]
    verbs      = ["get", "list"]
  }
}

resource "kubernetes_service_account" "gitlab_service_account" {
  metadata {
    name = "gitlab-service-account"
  }
}

resource "kubernetes_cluster_role_binding" "gitlab_role_binding" {
  metadata {
    name = "gitlab_role_binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-deployer"
  }

  subject {
    kind = "ServiceAccount"
    name = "gitlab-service-account"
  }
}

resource "kubernetes_service_account" "admin_service_account" {
  metadata {
    name = "admin-service-account"
  }
}

resource "kubernetes_cluster_role_binding" "admin_role_binding" {
  metadata {
    name = "admin_role_binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind = "ServiceAccount"
    name = "admin-service-account"
  }
}
